package it.sb.designpatterns.creational.singleton;

public class Singleton1 {
	private Singleton1() {
	}

	// I could have also set this property as public.
	private static final Singleton1 SINGLETON_INSTANCE = new Singleton1();

	public static Singleton1 getInstance() {
		return SINGLETON_INSTANCE;
	}
}
