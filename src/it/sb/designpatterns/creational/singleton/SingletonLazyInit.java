package it.sb.designpatterns.creational.singleton;

public class SingletonLazyInit {
	private SingletonLazyInit() {
	}

	private static SingletonLazyInit SINGLETON_INSTANCE = null;

	public static SingletonLazyInit getInstance() {
		synchronized (SingletonLazyInit.class) {
			if (SINGLETON_INSTANCE == null) {
				SINGLETON_INSTANCE = new SingletonLazyInit();
			}
		}

		return SINGLETON_INSTANCE;
	}
}
