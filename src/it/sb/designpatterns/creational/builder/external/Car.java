package it.sb.designpatterns.creational.builder.external;

public class Car {

	private String windshield;
	private String tyres;
	private String engine;
	private String chassis;

	public String getWindshield() {
		return windshield;
	}

	public void setWindshield(String windshield) {
		this.windshield = windshield;
	}

	public String getTyres() {
		return tyres;
	}

	public void setTyres(String tyres) {
		this.tyres = tyres;
	}

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	@Override
	public String toString() {
		return "Car [windshield=" + windshield + ", tyres=" + tyres + ", engine=" + engine + ", chassis=" + chassis
				+ "]";
	}
}
