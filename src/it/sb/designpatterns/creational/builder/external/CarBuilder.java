package it.sb.designpatterns.creational.builder.external;

import it.sb.designpatterns.creational.builder.external.Car;

public class CarBuilder {

	private String engine;
	private String windShield;
	private String chassis;
	private String tyres;

	public CarBuilder setEngine(String engine) {
		this.engine = engine;
		return this;
	}

	public CarBuilder setWindShield(String windShield) {
		this.windShield = windShield;
		return this;
	}

	public CarBuilder setChassis(String chassis) {
		this.chassis = chassis;
		return this;
	}

	public CarBuilder setTyres(String tyres) {
		this.tyres = tyres;
		return this;
	}

	public Car build() {
		// Any validation
		boolean valid = !chassis.isEmpty() && !engine.isEmpty() && !tyres.isEmpty() && !windShield.isEmpty();
		if (!valid) {
			throw new RuntimeException("Invalid object");
		}
		Car car = new Car();
		car.setChassis(chassis);
		car.setEngine(engine);
		car.setTyres(tyres);
		car.setWindshield(windShield);
		return car;
	}
}
