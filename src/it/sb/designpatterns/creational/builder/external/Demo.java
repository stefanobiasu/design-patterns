package it.sb.designpatterns.creational.builder.external;

import it.sb.designpatterns.creational.builder.external.Car;

public class Demo {

	public static void main(String[] args) {

		buildCar("chassis", "engine", "tyres", "windShield");
		buildCar("chassis", null, "tyres", "windShield");
	}

	private static void buildCar(String chassis, String engine, String tyres, String windShield) {
		try {
			Car car = new CarBuilder()//
					.setChassis(chassis)//
					.setEngine(engine)//
					.setTyres(tyres)//
					.setWindShield(windShield)//
					.build();
			System.out.println("Car built:" + car.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
