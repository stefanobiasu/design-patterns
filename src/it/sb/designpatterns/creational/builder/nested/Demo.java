package it.sb.designpatterns.creational.builder.nested;

public class Demo {

	public static void main(String[] args) {

		buildCar("chassis", "engine", "tyres", "windShield");
		buildCar("chassis", null, "tyres", "windShield");
	}

	private static void buildCar(String chassis, String engine, String tyres, String windShield) {
		try {
			Car car = new Car.builder()//
					.setChassis(chassis)//
					.setEngine(engine)//
					.setTyres(tyres)//
					.setWindshield(windShield)//
					.build();
			System.out.println("Car built:" + car.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
