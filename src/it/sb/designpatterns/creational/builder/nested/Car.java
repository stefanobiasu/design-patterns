package it.sb.designpatterns.creational.builder.nested;

public class Car {

	private String windshield;
	private String tyres;
	private String engine;
	private String chassis;

	public String getWindshield() {
		return windshield;
	}

	public void setWindshield(String windshield) {
		this.windshield = windshield;
	}

	public String getTyres() {
		return tyres;
	}

	public void setTyres(String tyres) {
		this.tyres = tyres;
	}

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	private Car() {
	};

	@Override
	public String toString() {
		return "Car [windshield=" + windshield + ", tyres=" + tyres + ", engine=" + engine + ", chassis=" + chassis
				+ "]";
	}

	public static class builder {
		private String windshield;
		private String tyres;
		private String engine;
		private String chassis;

		public builder setWindshield(String windShield) {
			this.windshield = windShield;
			return this;
		}

		public builder setTyres(String tyres) {
			this.tyres = tyres;
			return this;
		}

		public builder setEngine(String engine) {
			this.engine = engine;
			return this;
		}

		public builder setChassis(String chassis) {
			this.chassis = chassis;
			return this;
		}

		public Car build() {
			// Any validation
			boolean valid = !chassis.isEmpty() && !engine.isEmpty() && !tyres.isEmpty() && !windshield.isEmpty();
			if (!valid) {
				throw new RuntimeException("Invalid object");
			}
			Car car = new Car();
			car.setChassis(chassis);
			car.setEngine(engine);
			car.setTyres(tyres);
			car.setWindshield(windshield);
			return car;
		}
	}
}
