package it.sb.designpatterns.structural.adapter.byMethod;

public class Demo {
	public static void main(String[] args) {
		AmericanBuilding american = new AmericanBuilding();
		EnglishBuilding english = new EnglishBuilding();
		System.out.println("English building has " + Adapter.getStories(english) + " stories");
		System.out.println("American building has " + Adapter.getStoreys(american) + " storeys");
	}
}
