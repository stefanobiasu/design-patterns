package it.sb.designpatterns.structural.adapter.byMethod;

public class Adapter {

	public static int getStoreys(AmericanBuilding american) {
		return american.getStories();
	}

	public static int getStories(EnglishBuilding english) {
		return english.getStoreys();
	}

}
