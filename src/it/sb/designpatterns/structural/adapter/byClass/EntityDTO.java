package it.sb.designpatterns.structural.adapter.byClass;

public class EntityDTO {
	private String field;

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

}
