package it.sb.designpatterns.structural.adapter.byClass;

public class Demo {
	public static void main(String[] args) {

		EntityDTO dto = new EntityDTO();
		dto.setField("field");

		Adapter adapter = new Adapter();
		EntityDOC doc = adapter.adapt(dto);

		System.out.println("field in doc: " + doc.getField());
	}
}
