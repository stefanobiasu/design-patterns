package it.sb.designpatterns.structural.adapter.byClass;

public class Adapter {

	public EntityDOC adapt(EntityDTO dto) {
		EntityDOC doc = new EntityDOC();
		doc.setField(dto.getField());
		return doc;
	}
}
