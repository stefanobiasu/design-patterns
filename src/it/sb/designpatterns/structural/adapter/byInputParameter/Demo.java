package it.sb.designpatterns.structural.adapter.byInputParameter;

public class Demo {
	public static void main(String[] args) {
		IPhone iPhone = new IPhone();
		LightningCable lCable = new LightningCable();

		// simple use
		iPhone.connect(lCable);

		AndroidPhone phone = new AndroidPhone();
		MicroUSBCable mCable = new MicroUSBCable();

		// simple use
		phone.connect(mCable);

		// adapter use
		Adapter adapter = new Adapter();
		adapter.connectAndroid(phone, lCable);
		adapter.connectIPhone(iPhone, mCable);

	}
}
