package it.sb.designpatterns.structural.adapter.byInputParameter;

public class AndroidPhone {

	public void connect(MicroUSBCable cable) {
		System.out.println("Android connected to " + cable.getClass().toString());
	}
}
