package it.sb.designpatterns.structural.adapter.byInputParameter;

public class Adapter {

	public void connectIPhone(IPhone phone, MicroUSBCable cable) {
		LightningCable lCable = getLightning(cable);
		phone.connect(lCable);
	}

	private LightningCable getLightning(MicroUSBCable cable) {
		System.out.println("LightningCable out of MicroUSBcable");
		return new LightningCable();
	}

	public void connectAndroid(AndroidPhone phone, LightningCable cable) {
		MicroUSBCable mCable = getMicroUSBCable(cable);
		phone.connect(mCable);
	}

	private MicroUSBCable getMicroUSBCable(LightningCable cable) {
		System.out.println("MicroUSBCable out of Lightningcable");
		return new MicroUSBCable();
	}
}
