package it.sb.designpatterns.structural.adapter.byInputParameter;

public class IPhone {

	public void connect(LightningCable cable) {
		System.out.println("IPhone connected to " + cable.getClass().toString());
	}
}
