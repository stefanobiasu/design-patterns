package it.sb.designpatterns.behavioral.template;

/**
 * An implementation of Golf Game.
 * 
 * @author stefano
 *
 */
public class Golf extends Game {

	@Override
	public void start() {
		System.out.println("Golf game has started!");
	}

	@Override
	public void end() {
		System.out.println("Golf game has ended!");
	}

}
