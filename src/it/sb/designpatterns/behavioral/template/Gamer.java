package it.sb.designpatterns.behavioral.template;

public class Gamer {
	public static void main(String[] args) {
		// Create a golf game
		Game golf = new Golf();
		//play it
		golf.play();
		// create a soccer game
		Game soccer = new Soccer();
		// play it
		soccer.play();
	}
}
