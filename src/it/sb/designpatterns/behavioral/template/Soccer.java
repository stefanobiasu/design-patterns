package it.sb.designpatterns.behavioral.template;

public class Soccer extends Game {

	@Override
	public void start() {
		System.out.println("Soccer game has started!");
	}

	@Override
	public void end() {
		System.out.println("Soccer game has ended!");
	}

}
