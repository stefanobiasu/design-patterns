package it.sb.designpatterns.behavioral.template;

/**
 * An abstract class representing a game.
 * 
 * @author stefano
 *
 */
public abstract class Game {

	/**
	 * This method will be implemented by subclasses
	 */
	public abstract void start();

	/**
	 * This method will be implemented by subclasses
	 */
	public abstract void end();

	/**
	 * main method of Game, will be called by any Gamer.
	 */
	public final void play() {
		start();
		end();
	}
}
