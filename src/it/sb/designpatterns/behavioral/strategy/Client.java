package it.sb.designpatterns.behavioral.strategy;

/**
 * Class using addition and substraction operation strategies.
 * 
 * @author stefano
 *
 */
public class Client {

	public static void main(String[] args) throws Exception {

		// define inputs
		double input1 = 1;
		double input2 = 2;

		// create StrategyImpl for addition
		StrategyImpl addStrategyImpl = new StrategyImpl(AddOperationStrategy.class);
		// get addition result
		double addResult = addStrategyImpl.perform(input1, input2);
		System.out.println("Addition result: " + addResult);

		// create StrategyImpl for subtraction
		StrategyImpl subtractStrategyImpl = new StrategyImpl(SubtractOperationStrategy.class);
		// get substract result
		double substractResult = subtractStrategyImpl.perform(input1, input2);
		System.out.println("Subtraction result: " + substractResult);
	}
}
