package it.sb.designpatterns.behavioral.strategy;

/**
 * OperationStrategy class implementing the addition.
 * 
 * @author stefano
 *
 */
public class AddOperationStrategy implements OperationStrategy {

	@Override
	public double performOperation(double input1, double input2) {
		return input1 + input2;
	}

}
