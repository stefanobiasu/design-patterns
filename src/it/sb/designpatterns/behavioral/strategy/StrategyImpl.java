package it.sb.designpatterns.behavioral.strategy;

/**
 * Class utility for OperationStrategy usage.
 * 
 * @author stefano
 *
 */
public class StrategyImpl {

	private OperationStrategy strategy;

	/**
	 * Constructor.
	 * 
	 * @param strategyClass The strategy class to use.
	 * @throws Exception In case of strategy class implementation failure.
	 */
	public StrategyImpl(Class<? extends OperationStrategy> strategyClass) throws Exception {
		strategy = strategyClass.newInstance();
	}

	/**
	 * Calls operationStrategy.performOperation() and return the result.
	 * 
	 * @param input1 first operand.
	 * @param input2 second operand.
	 * @return The operation result.
	 */
	public double perform(double input1, double input2) {
		return strategy.performOperation(input1, input2);
	}
}
