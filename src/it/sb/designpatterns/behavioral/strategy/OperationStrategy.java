package it.sb.designpatterns.behavioral.strategy;

/**
 * Strategy interface to define an operation using two doubles as input
 * parameters.
 * 
 * @author stefano
 *
 */
public interface OperationStrategy {

	/**
	 * Method to perform the operation and release the operation result.
	 * 
	 * @param input1 first operand.
	 * @param input2 second operand.
	 * @return The result of the operation.
	 */
	public double performOperation(double input1, double input2);
}
