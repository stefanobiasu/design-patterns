package it.sb.designpatterns.behavioral.strategy;

/**
 * OperationStrategy class implementing subtraction.
 * 
 * @author stefano
 *
 */
public class SubtractOperationStrategy implements OperationStrategy {

	@Override
	public double performOperation(double input1, double input2) {
		return input1 - input2;
	}
}
