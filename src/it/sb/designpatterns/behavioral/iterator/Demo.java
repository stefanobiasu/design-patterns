package it.sb.designpatterns.behavioral.iterator;

import java.util.Arrays;

public class Demo {

	public static void main(String[] args) {

		NameRepository repo = new NameRepository(Arrays.asList("Mike", "Ann", "Gilbert"));
		while (repo.hasNext()) {
			System.out.println(repo.next());
		}
	}
}
