package it.sb.designpatterns.behavioral.iterator;

public interface Iterator<T> {

	boolean hasNext();

	T next();
}
