package it.sb.designpatterns.behavioral.iterator;

import java.util.ArrayList;
import java.util.List;

public class NameRepository implements Iterator<String> {

	private List<String> repo = new ArrayList<String>();
	private int index = 0;

	public NameRepository(List<String> repo) {
		this.repo = repo;
	}

	@Override
	public boolean hasNext() {
		return index < repo.size();
	}

	@Override
	public String next() {
		return repo.get(index++);
	}
}
